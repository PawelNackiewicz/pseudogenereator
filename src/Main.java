import model.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) throws IOException {

        FileWriter fileWriter = new FileWriter("inserts.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);

        //generator insertow
        int length = 100;
        for (int i = 0; i < length; i++) {
            Adres2 adres2 = new Adres2();
            printWriter.println(adres2.toString());
        }

        printWriter.close();
    }
}

