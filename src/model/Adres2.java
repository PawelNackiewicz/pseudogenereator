package model;

import java.util.Arrays;
import java.util.Random;

public class Adres2 {

    Random random = new Random();

    String country = "Pol";
    String city;
    String street;
    int houseNumber;

    String [] potentialCitys={
        "Warszawa","Krakow","Rzeszow","Lodz","Opole","Gdansk"
    };
    String [] potentialStreet={
            "Mala","Duza","Ogrodowa","Wielka","Klonowa"
    };

    public Adres2() {
        this.city=randomCity();
        this.street=randomStreet();
        this.houseNumber=randomNumber();
    }

    private String randomCity(){
        return potentialCitys[random.nextInt(potentialCitys.length)];
    }

    private String randomStreet(){
        return potentialStreet[random.nextInt(potentialStreet.length)];
    }
    private int randomNumber(){
        return random.nextInt(100)+1;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    @Override
    public String toString() {
        return "INSERT INTO ADRES2 (KRAJ,MIASTO,ULICA,NR_DOMU) VALUES" + "('Polska','" + this.getCity() + "','" + this.getStreet() + "','" + this.getHouseNumber()+ "');";
    }
}